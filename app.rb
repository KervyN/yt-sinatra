require 'sinatra'
require 'resolv'

set :port, 16002
set :bind, '127.0.0.1'
set :public_folder, File.dirname(__FILE__) + '/static'

before do
  @allowed_ips = ['192.168.88.', '127.0.']
  @allowed_ips << Resolv.getaddress('borisb.tech')

  unless @allowed_ips.any? { |ip| request.ip.to_s.include?(ip) }
    halt 418, erb(:goaway)
  end

  #if localip != request.ip and !(request.ip.to_s.include? '192.168.' or request.ip.to_s.include? '127.0.')
  #  halt(404, request.ip)
  #end
end

get '/' do
  erb :yt
end

post '/download' do
  random = (0...3).map { (65 + rand(26)).chr }.join
  params[:playlist] == 'on' ? (playlist = 'playlist') : (playlist = 'video')
  params[:yturl].empty? ? (url = 'undef') : (url = params[:yturl])
  @@pid = Process.spawn("/srv/webapps/youtube-dl/shared/bin/youtube-dl --config-location /srv/webapps/youtube-dl/shared/config/youtube-dl.conf #{url} > /srv/webapps/youtube-dl/shared/log/production-#{random}")
  Process.detach @@pid
  sleep(2)
  redirect to("https://yt.borisb.tech/status/#{random}")
end

get '/status/:id' do
  @logfile = File.read("/srv/webapps/youtube-dl/shared/log/production-#{params[:id]}")
  erb :downlog
end
